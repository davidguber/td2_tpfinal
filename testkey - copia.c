//gcc testkey.c soft_i2c.c soft_lcd.c -o testkey -lwiringPi -lncurses -I/usr/include/mysql -lmysqlclient

#include <wiringPi.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "soft_lcd.h"

#include <stdlib.h>
#include <mysql.h>

#define ROWS 4
#define COLS 4

#define LOCK 7
#define LOCK_ACTIVE HIGH
#define LOCK_INACTIVE LOW

#define DELAY_KEYPAD 140

#define MAX_PASS_LEN 4

char User[5];

char Pass[5];

int k = 0;
int l = 0;
int m = 0;

char query_str[256];

char pressedKey = '\0';
int rowPins[ROWS] = {22, 23, 24, 25};
int colPins[COLS] = {21, 27, 28, 29};

char keys[ROWS][COLS] = {
   {'1', '2', '3', 'A'},
   {'4', '5', '6', 'B'},
   {'7', '8', '9', 'C'},
   {'*', '0', '#', 'D'}
};

void init_keypad()
{
   for (int c = 0; c < COLS; c++)
   {
      pinMode(colPins[c], OUTPUT);   
      digitalWrite(colPins[c], HIGH);
   }

   for (int r = 0; r < ROWS; r++)
   {
      pinMode(rowPins[r], INPUT);   
      pullUpDnControl(rowPins[r], PUD_UP);
   }
}

int findLowRow()
{
   for (int r = 0; r < ROWS; r++)
   {
      if (digitalRead(rowPins[r]) == LOW)
         return r;
   }

   return -1;
}

char get_key()
{
   int rowIndex;

   for (int c = 0; c < COLS; c++)
   {
      digitalWrite(colPins[c], LOW);

      rowIndex = findLowRow();
      if (rowIndex > -1)
      {
         if (!pressedKey)
            pressedKey = keys[rowIndex][c];
         return pressedKey;
      }

      digitalWrite(colPins[c], HIGH);
   }

   pressedKey = '\0';

   return pressedKey;
}

int main(void) 
{
   wiringPiSetup();

   init_keypad();
   lcd_t *lcd = lcd_create(9, 8, 0x27, 2);
	if (lcd == NULL) {
		printf("Cannot set-up LCD.\n");
	}
	pinMode(LOCK, OUTPUT);
	
	/*MYSQL*/
	MYSQL *conn;
    MYSQL_RES *res;
    MYSQL_ROW row;
	MYSQL_ROW row2;
    char *server = "localhost";
    char *user = "admin";
    char *password = "tomasyvictor"; /* set me first */
    char *database = "control_acceso";
	conn = mysql_init(NULL);
	/* Connect to database */
    if (!mysql_real_connect(conn, server, user, password, database, 0, NULL, 0)) 
	{
        fprintf(stderr, "%s\n", mysql_error(conn));
        exit(1);
    }
	
	lcd_print(lcd, "Electronic Door");
    lcd_pos(lcd, 1, 0);
    lcd_print(lcd, "Lock Using RPI ");
	delay(5000);
	lcd_pos(lcd, 0, 0);
   while(m!=1)
   {
      k = 0;
	  l = 0;
	  User[0] = '0';
	  User[1] = '0';
	  User[2] = '0';
	  User[3] = '0';
	  User[4] = '\0';
	  
	  Pass[0] = '0';
	  Pass[1] = '0';
	  Pass[2] = '0';
	  Pass[3] = '0';
	  Pass[4] = '\0';
	  
	  lcd_clear(lcd);
	  lcd_print(lcd, "Usuario");
	  lcd_pos(lcd, 1, 0);
	  
	  while(k<4)
		{
			char x = get_key();
		
			if (x)
			{
				User[k] = x;
				lcd_pos(lcd, 1, k);
				lcd_printf(lcd, "%c", x);
				k++;
			}
			else
			delay(DELAY_KEYPAD);
		}
		
		if (strcmp(User,"9999")==0) 
		{
			m=1;
			break;
		}
		
		sprintf(query_str, "SELECT `ID` FROM `cda_usuarios` WHERE `Usuario`='%s'", User);
		if (mysql_query(conn, query_str)) 
		{
			fprintf(stderr, "%s\n", mysql_error(conn));
			exit(1);
		}
		
		res = mysql_use_result(conn);
		if((row = mysql_fetch_row(res)) != NULL)
		{
		  lcd_clear(lcd);
		  lcd_print(lcd, "Contraseña");
		  lcd_pos(lcd, 1, 0);
		  while(l<MAX_PASS_LEN)
			{
				char x = get_key();
			
				if (x)
				{
					Pass[l] = x;
					lcd_pos(lcd, 1, l);
					lcd_printf(lcd, "*");
					l++;
				}
				else
				delay(DELAY_KEYPAD);
			}
			mysql_free_result(res);
			sprintf(query_str, "SELECT `ID` FROM `cda_usuarios` WHERE `Usuario`='%s' AND `Password` = MD5('%s')", User, Pass);
			if (mysql_query(conn, query_str)) 
			{
				fprintf(stderr, "%s\n", mysql_error(conn));
				exit(1);
			}
			res = mysql_use_result(conn);
			if((row2 = mysql_fetch_row(res)) != NULL)
			{
				mysql_free_result(res);
				digitalWrite(LOCK, LOCK_ACTIVE);
				sprintf(query_str, "INSERT INTO `logs_entrada`(`Usuario`) VALUES ('%s')", User);
				if (mysql_query(conn, query_str)) 
				{
					fprintf(stderr, "%s\n", mysql_error(conn));
					exit(1);
				}
				lcd_clear(lcd);
				lcd_print(lcd, "Puerta abierta");
				delay(5000);
				digitalWrite(LOCK, LOCK_INACTIVE);
			}
			else
			{
				lcd_clear(lcd);
				lcd_print(lcd, "ERROR:Contraseña");
				lcd_pos(lcd, 1, 0);
				lcd_print(lcd, "Invalida");
				mysql_free_result(res);
				delay(2000);
			}
			
		}
		else
		{
			lcd_clear(lcd);
			lcd_print(lcd, "ERROR: Usuario");
			lcd_pos(lcd, 1, 0);
			lcd_print(lcd, "Invalido");
			mysql_free_result(res);
			delay(2000);
		}
   }
   lcd_destroy(lcd);
   mysql_free_result(res);
   mysql_close(conn);

   return 0;
}
