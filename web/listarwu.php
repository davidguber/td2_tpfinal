<!doctype html>
<html lang="en">
  <?php
	require("includes/config.php");
	if(isset($_SESSION['Logeado']))
	{ 
	include("includes/head.php");
  ?>
  
  <body>
  <?php
	include("includes/navbar.php");
	?>

<main role="main">

  <div class="container">
    <!-- Example row of columns -->
    <div class="row">
      <h1 class="page-header">Lista de usuarios web</h1>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
				  <th>Usuario</th>
                  <th>Email</th>
				  <th>Borrar</th>
				  </tr>
              </thead>
              <tbody>
			  <?php
			  
				$cant_pags = 20;/*Cantidad mostrado en pagina*/
			  
				if(!empty($_GET['p']))
				{
					if(is_numeric($_GET['p'])) 
					{
						if($_GET['p'] >= 1)
						{
							$pag = mysqli_real_escape_string($con, $_GET['p']);
						}
						else $pag = 1;
					}
					else $pag = 1;
				}
				else $pag = 1;
				$snum = ($pag-1)*$cant_pags;/*Cantidad mostrado en pagina*/
				$query = "SELECT * FROM `usuarios_web` WHERE 1 ORDER BY `ID` DESC LIMIT $snum,$cant_pags";/*Cantidad mostrado en pagina*/
				$result=mysqli_query($con, $query);
				
				$num=mysqli_num_rows($result);

				if($num >= 1)
				{
					while($row = mysqli_fetch_assoc($result)) 
					{
						$ID = $row['ID'];
						$UsuarioN = html_entity_decode($row['Usuario']);
						$Email = html_entity_decode($row['Mail']);
						echo "<tr>
								  <td>".$ID."</td>
								  <td>".$UsuarioN."</td>
								  <td>".$Email."</td>
								  <td><a href='borrarwu.php?u=".$UsuarioN."'><i class='fas fa-user-times'></i></a></td>
							  </tr>";
					}
					mysqli_free_result($result);
				}
				else echo "<h1>No se han encontrado registros de ingresos.</h1>";
				
				?>
              </tbody>
            </table>
          </div>
		  
		  <!-- Pager -->
				<?php
				if($num >= 1)
				{
					echo "
					<nav class='blog-pagination'>
					";
						$p1snum = $pag*$cant_pags;/*Cantidad mostrado en pagina*/
						$p1query = "SELECT `ID` FROM `usuarios_web` ORDER BY `ID` DESC LIMIT $p1snum,$cant_pags";/*Cantidad mostrado en pagina*/
						$p1result=mysqli_query($con, $p1query);
						$p1num=mysqli_num_rows($p1result);
						if($p1num >= 1) 
						{
							$p1pag = $pag+1;
						}
						mysqli_free_result($p1result);
						
						if($pag >= 2)
						{
							$p2pag = $pag-1;
							echo "
							<a class='btn btn-outline-primary' href='listaru.php?p=$p2pag'>&larr; Más Nuevo</a>
							";
						}
						
						if($p1num >= 1) 
						{
							echo "
							<a class='btn btn-outline-primary' href='listaru.php?p=$p1pag'>Más Antiguo &rarr;</a>
							";
						}
						
						
						
					echo "
					</nav>
					";
				}
				?>
		  
    </div>

    <hr>

  </div> <!-- /container -->

	</main>
	<!-- Footer -->
    <?php
	include("includes/footer.php");
	?>

    <!-- Scripts -->
	<?php
	include("includes/scripts.php");
	mysqli_close($con);
	?>
	</body>
	
	<?php
	}
	else
	{
	  include("includes/head2.php");
	?>
	  <body>
		<div class="container">
		  <form class="form-signin">
			<div class='well'>
				<h2><b>Error</b></h2></br></br>
				<div class='alert alert-danger' role='alert'>No ha ingresado al sistema.</div>
				<meta http-equiv='Refresh' content='4;url=login.php' />
			</div>
		  </form>

		</div> <!-- /container -->
	  </body>
	<?php
	}
	?>
</html>