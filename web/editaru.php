<!doctype html>
<html lang="en">
  <?php
	require("includes/config.php");
	if(isset($_SESSION['Logeado']))
	{ 
	include("includes/head.php");
  ?>
  
  <body>
  <?php
	include("includes/navbar.php");
	?>

<main role="main">

  <div class="container">
    <!-- Example row of columns -->
    <div class="row">
      <h1 class="page-header">Modificar usuario</h1>
          <?php
			if(!empty($_GET['u']) && empty($_POST))
			{
				$NURL = mysqli_real_escape_string($con, $_GET['u']);
				$query = "SELECT * FROM `cda_usuarios` WHERE `Usuario`='$NURL' LIMIT 0,1";
				$result=mysqli_query($con, $query);
				$num=mysqli_num_rows($result);

				if($num == 1)
				{
					$row=mysqli_fetch_array($result,MYSQLI_ASSOC);
					$ID = $row['ID'];
					$UsuarioN = $row['Usuario'];
					$Nombre = html_entity_decode($row['Nombre']);
					$Apellido = html_entity_decode($row['Apellido']);
					$Telefono = html_entity_decode($row['Telefono']);
					$Email = html_entity_decode($row['Email']);
					echo "
					<div class='jumbotron'>
					<div class='container'>
					  <h1 class='display-3'><b>Editar usuario</b></h1>
						<form action='editaru.php' method='post'>
							<h4>Usuario</h4>
							<input class='form-control' type='text' name='UsuarioN' value='".$UsuarioN."'></br>
							<h4>Contraseña</h4>
							<input class='form-control' type='text' name='Password' value=''></br>
							<h4>Nombre</h4>
							<input class='form-control' type='text' name='Nombre' value='".$Nombre."'></br>
							<h4>Apellido</h4>
							<input class='form-control' type='text' name='Apellido' value='".$Apellido."'></br>
							<h4>Telefono</h4>
							<input class='form-control' type='text' name='Telefono' value='".$Telefono."'></br>
							<h4>Email</h4>
							<input class='form-control' type='text' name='Email' value='".$Email."'></br>
							<input type='hidden' name='ID' value='".$ID."'>
							<input type='hidden' name='Usuario' value='".$UsuarioN."'>
							<input type='submit' class='btn btn-info' value='Confirmar' /> 
						</form>
					</div>
				</div>
					";
				}
				else
				{
					echo "
					<div class='jumbotron'>
					<div class='container'>
					<div class='alert alert-danger' role='alert'>
						  <h4 class='alert-heading'>¡ERROR!</h4>
						  <p>Hubo un error en la página. Por favor vuelva a <a href='listaru.php'>intentarlo nuevamente</a>.</p>
					</div>
					</div>
					</div>
					<meta http-equiv='Refresh' content='4;url=listaru.php' />
					";
				}
				mysqli_free_result($result);
				
				
			}
			elseif(!empty($_POST) && empty($_GET['u']))
			{
				if(!empty($_POST['ID']) && !empty($_POST['Usuario']) && !empty($_POST['UsuarioN']) && !empty($_POST['Nombre']) && !empty($_POST['Apellido']) && !empty($_POST['Telefono']) && !empty($_POST['Email']))
				{
					$NuevoUsuario = htmlentities(mysqli_real_escape_string($con, $_POST['UsuarioN']));
					$NuevoPassword = htmlentities(mysqli_real_escape_string($con, $_POST['Password']));
					$NuevoNombre = htmlentities(mysqli_real_escape_string($con, $_POST['Nombre']));
					$NuevoApellido = htmlentities(mysqli_real_escape_string($con, $_POST['Apellido']));
					$NuevoTelefono = htmlentities(mysqli_real_escape_string($con, $_POST['Telefono']));
					$NuevoEmail = htmlentities(mysqli_real_escape_string($con, $_POST['Email']));
					$ID = mysqli_real_escape_string($con, $_POST['ID']);
					$Usuario = mysqli_real_escape_string($con, $_POST['Usuario']);
					
					if (strcmp($Usuario, $NuevoUsuario) == 0)
					{
						$cresult == false;
					}
					else 
					{
						$cquery = "SELECT `ID` FROM `cda_usuarios` WHERE `Usuario`='$NuevoUsuario' LIMIT 1";
						$cresult=mysqli_query($con, $cquery);
					}
					
					
					
					if($cresult == false)
					{
						if(empty($_POST['Password']))
						{
							$equery = "UPDATE `cda_usuarios` SET `Usuario`='$NuevoUsuario',`Nombre`='$NuevoNombre',`Apellido`='$NuevoApellido',`Telefono`='$NuevoTelefono',`Email`='$NuevoEmail' WHERE `ID`='$ID' LIMIT 1";
						}
						else
						{
							$equery = "UPDATE `cda_usuarios` SET `Usuario`='$NuevoUsuario',`Password`=MD5('$NuevoPassword'),`Nombre`='$NuevoNombre',`Apellido`='$NuevoApellido',`Telefono`='$NuevoTelefono',`Email`='$NuevoEmail' WHERE `ID`='$ID' LIMIT 1";
						}
						
						$eresult=mysqli_query($con, $equery);
						if($eresult == true)
						{
							echo "
							<div class='jumbotron'>
							<div class='container'>
							<div class='alert alert-success' role='alert'>
							  <h4 class='alert-heading'>¡Exito!</h4>
							  <p>Se ha modificado el usuario con éxito. Puede volver a la <a href='index.php'>lista de usuarios</a>.</p>
							</div>
							</div>
							</div>
							<meta http-equiv='Refresh' content='4;url=index.php' />
							";
						}
						else
						{
							echo "
							<div class='jumbotron'>
							<div class='container'>
							<div class='alert alert-danger' role='alert'>
								  <h4 class='alert-heading'>¡ERROR!</h4>
								  <p>Hubo un error en la página. Por favor vuelva a <a href='listaru.php'>intentarlo nuevamente</a>.</p>
							</div>
							</div>
							</div>
							<meta http-equiv='Refresh' content='4;url=listaru.php' />
							";
						}
						mysqli_free_result($eresult);
					}
					else
					{
						echo "
						<div class='jumbotron'>
						<div class='container'>
						<div class='alert alert-danger' role='alert'>
							  <h4 class='alert-heading'>¡ERROR!</h4>
							  <p>No puede cambiar un usuario a uno ya existente. Por favor vuelva a <a href='listaru.php'>intentarlo nuevamente</a>.</p>
						</div>
						</div>
						</div>
						<meta http-equiv='Refresh' content='4;url=listaru.php' />
						";
					}
					mysqli_free_result($cresult);
				}
				else
				{
					echo "
					<div class='jumbotron'>
					<div class='container'>
					<div class='alert alert-danger' role='alert'>
						  <h4 class='alert-heading'>¡ERROR!</h4>
						  <p>Hay campos vacíos. Por favor vuelva a <a href='listaru.php'>intentarlo nuevamente</a>.</p>
					</div>
					</div>
					</div>
					<meta http-equiv='Refresh' content='4;url=listaru.php' />
					";
				}
			}
			else
			{
				echo "
				<div class='jumbotron'>
				<div class='container'>
				<div class='alert alert-danger' role='alert'>
					  <h4 class='alert-heading'>¡ERROR!</h4>
					  <p>Hubo un error en la página. Por favor vuelva a <a href='listaru.php'>intentarlo nuevamente</a>.</p>
				</div>
				</div>
				</div>
				<meta http-equiv='Refresh' content='4;url=listaru.php' />
				";
			}
			?>
		  
    </div>

    <hr>

  </div> <!-- /container -->

	</main>
	<!-- Footer -->
    <?php
	include("includes/footer.php");
	?>

    <!-- Scripts -->
	<?php
	include("includes/scripts.php");
	mysqli_close($con);
	?>
	</body>
	
	<?php
	}
	else
	{
	  include("includes/head2.php");
	?>
	  <body>
		<div class="container">
		  <form class="form-signin">
			<div class='well'>
				<h2><b>Error</b></h2></br></br>
				<div class='alert alert-danger' role='alert'>No ha ingresado al sistema.</div>
				<meta http-equiv='Refresh' content='4;url=login.php' />
			</div>
		  </form>

		</div> <!-- /container -->
	  </body>
	<?php
	}
	?>
</html>