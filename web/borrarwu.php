<!doctype html>
<html lang="en">
  <?php
	require("includes/config.php");
	if(isset($_SESSION['Logeado']))
	{ 
	include("includes/head.php");
  ?>
  
  <body>
  <?php
	include("includes/navbar.php");
	?>

<main role="main">

  <div class="container">
    <!-- Example row of columns -->
    <div class="row">
      <h1 class="page-header">Eliminar usuario web</h1>
          <?php
			if(!empty($_GET['u']) && empty($_POST))
			{
				$NURL = mysqli_real_escape_string($con, $_GET['u']);
				$query = "SELECT `ID` FROM `usuarios_web` WHERE `Usuario`='$NURL' LIMIT 0,1";
				$result=mysqli_query($con, $query);
				$num=mysqli_num_rows($result);

				if($num == 1)
				{
					$row=mysqli_fetch_array($result,MYSQLI_ASSOC);
					$UID = $row['ID'];
					echo "
					<div class='jumbotron'>
					<div class='container'>
					<div class='alert alert-danger' role='alert'>
						  <h4 class='alert-heading'>¿Confirma que desea eliminar el usuario web ".$UsuarioN."?</h4>
						  <form action='borrarwu.php' method='post'>
							<input type='radio' value='yes' name='dconfirm'> Si</br></br>
							<input type='radio' value='no' name='dconfirm' checked> No</br></br>
							<input type='hidden' name='ID' value='$UID'>
							<input type='submit' class='btn btn-info' value='Confirmar' /> 
						</form>
					</div>
					</div>
					</div>
					";
				}
				else
				{
					echo "
					<div class='jumbotron'>
					<div class='container'>
					<div class='alert alert-danger' role='alert'>
						  <h4 class='alert-heading'>¡ERROR!</h4>
						  <p>Hubo un error en la página. Por favor vuelva a <a href='listarwu.php'>intentarlo nuevamente</a>.</p>
					</div>
					</div>
					</div>
					<meta http-equiv='Refresh' content='4;url=listaru.php' />
					";
				}
				mysqli_free_result($result);
				
				
			}
			elseif(!empty($_POST) && empty($_GET['u']))
			{
				if(!empty($_POST['ID'] && !empty($_POST['dconfirm'])))
				{
					if($_POST['dconfirm'] == "yes")
					{
						$NID = mysqli_real_escape_string($con, $_POST['ID']);
						$dquery = "DELETE FROM `usuarios_web` WHERE `ID`='$NID' LIMIT 1";
						$dresult=mysqli_query($con, $dquery);
						if($dresult == true)
						{
							echo "
							<div class='jumbotron'>
							<div class='container'>
							<div class='alert alert-success' role='alert'>
							  <h4 class='alert-heading'>¡Exito!</h4>
							  <p>Se ha eliminado el usuario con éxito. Puede volver al <a href='index.php'>inicio</a>.</p>
							</div>
							</div>
							</div>
							<meta http-equiv='Refresh' content='4;url=index.php' />
							";
						}
						else
						{
							echo "
							<div class='jumbotron'>
							<div class='container'>
							<div class='alert alert-danger' role='alert'>
								  <h4 class='alert-heading'>¡ERROR!</h4>
								  <p>Hubo un error en la página. Por favor vuelva a <a href='listarwu.php'>intentarlo nuevamente</a>.</p>
							</div>
							</div>
							</div>
							<meta http-equiv='Refresh' content='4;url=listaru.php' />
							";
						}
					}
					else
					{
						echo "
						<div class='jumbotron'>
						<div class='container'>
						<div class='alert alert-danger' role='alert'>
							  <h4 class='alert-heading'>¡ERROR!</h4>
							  <p>No se confirmó la eliminacion del usuario web. Puede volver a la <a href='listarwu.php'>lista de usuarios</a>.</p>
						</div>
						</div>
						</div>
						<meta http-equiv='Refresh' content='4;url=listarwu.php' />
						";
					}
				}
				else
				{
					echo "
					<div class='jumbotron'>
					<div class='container'>
					<div class='alert alert-danger' role='alert'>
						  <h4 class='alert-heading'>¡ERROR!</h4>
						  <p>Hubo un error en la página. Por favor vuelva a <a href='listarwu.php'>intentarlo nuevamente</a>.</p>
					</div>
					</div>
					</div>
					<meta http-equiv='Refresh' content='4;url=listarwu.php' />
					";
				}
			}
			else
			{
				echo "
				<div class='jumbotron'>
				<div class='container'>
				<div class='alert alert-danger' role='alert'>
					  <h4 class='alert-heading'>¡ERROR!</h4>
					  <p>Hubo un error en la página. Por favor vuelva a <a href='listarwu.php'>intentarlo nuevamente</a>.</p>
				</div>
				</div>
				</div>
				<meta http-equiv='Refresh' content='4;url=listarwu.php' />
				";
			}
			?>
		  
    </div>

    <hr>

  </div> <!-- /container -->

	</main>
	<!-- Footer -->
    <?php
	include("includes/footer.php");
	?>

    <!-- Scripts -->
	<?php
	include("includes/scripts.php");
	mysqli_close($con);
	?>
	</body>
	
	<?php
	}
	else
	{
	  include("includes/head2.php");
	?>
	  <body>
		<div class="container">
		  <form class="form-signin">
			<div class='well'>
				<h2><b>Error</b></h2></br></br>
				<div class='alert alert-danger' role='alert'>No ha ingresado al sistema.</div>
				<meta http-equiv='Refresh' content='4;url=login.php' />
			</div>
		  </form>

		</div> <!-- /container -->
	  </body>
	<?php
	}
	?>
</html>