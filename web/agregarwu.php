<!doctype html>
<html lang="en">
  <?php
	require("includes/config.php");
	if(isset($_SESSION['Logeado']))
	{ 
	include("includes/head.php");
  ?>
  
  <body>
  <?php
	include("includes/navbar.php");
	?>

<main role="main">

  <div class="container">
    <!-- Example row of columns -->
    <div class="row">
      <?php
			if(empty($_POST))
			{
				echo "
				<div class='jumbotron'>
					<div class='container'>
					  <h1 class='display-3'><b>Nuevo usuario web</b></h1>
						<form action='agregarwu.php' method='post'>
							<h4>Usuario</h4>
							<input class='form-control' type='text' name='Usuario' value=''></br>
							<h4>Contraseña</h4>
							<input class='form-control' type='text' name='Password' value=''></br>
							<h4>Email</h4>
							<input class='form-control' type='text' name='Email' value=''></br>
							<input type='submit' class='btn btn-info' value='Confirmar' /> 
						</form>
					</div>
				</div>
				";
			}
			else if(!empty($_POST))
			{
				if(!empty($_POST['Usuario']) && !empty($_POST['Password']) && !empty($_POST['Email']))
				{
					$NuevoUsuario = htmlentities(mysqli_real_escape_string($con, $_POST['Usuario']));
					$NuevoPassword = htmlentities(mysqli_real_escape_string($con, $_POST['Password']));
					$NuevoEmail = htmlentities(mysqli_real_escape_string($con, $_POST['Email']));
					$nquery = "INSERT INTO `usuarios_web`(`Usuario`, `Password`, `Mail`) VALUES ('$NuevoUsuario',MD5('$NuevoPassword'),'$NuevoEmail')";
					$nresult=mysqli_query($con, $nquery);
					if($nresult == true)
					{
						echo "</br>
						<div class='jumbotron'>
						<div class='container'>
						<div class='alert alert-success' role='alert'>
						  <h4 class='alert-heading'>¡Exito!</h4>
						  <p>Se ha creado el usuario con éxito. Puede volver al <a href='index.php'>inicio</a>.</p>
						</div>
						</div>
						</div>
						<meta http-equiv='Refresh' content='4;url=index.php' />
						";
					}
					else
					{
						echo "</br>
						<div class='jumbotron'>
						<div class='container'>
						<div class='alert alert-danger' role='alert'>
						  <h4 class='alert-heading'>¡ERROR!</h4>
						  <p>Hubo un error en la página. Por favor vuelva a <a href='agregarwu.php'>intentarlo nuevamente</a>.</p>
						</div>
						</div>
						</div>
						<meta http-equiv='Refresh' content='4;url=agregarwu.php' />
						";
					}
					mysqli_free_result($nresult);
				}
				else
				{
					echo "
					<div class='jumbotron'>
					<div class='container'>
					<div class='alert alert-danger' role='alert'>
						  <h4 class='alert-heading'>¡ERROR!</h4>
						  <p>Hubo un error en la página. Por favor vuelva a <a href='agregarwu.php'>intentarlo nuevamente</a>.</p>
					</div>
					</div>
					</div>
					<meta http-equiv='Refresh' content='4;url=agregarwu.php' />
					";
				}
			}
			else
			{
				echo "
				<div class='jumbotron'>
				<div class='container'>
				<div class='alert alert-danger' role='alert'>
					  <h4 class='alert-heading'>¡ERROR!</h4>
					  <p>Hubo un error en la página. Por favor vuelva a <a href='agregarwu.php'>intentarlo nuevamente</a>.</p>
				</div>
				</div>
				</div>
				<meta http-equiv='Refresh' content='4;url=agregarwu.php' />
				";
			}
			?>  
    </div>

    <hr>

  </div> <!-- /container -->

	</main>
	<!-- Footer -->
    <?php
	include("includes/footer.php");
	?>

    <!-- Scripts -->
	<?php
	include("includes/scripts.php");
	mysqli_close($con);
	?>
	</body>
	
	<?php
	}
	else
	{
	  include("includes/head2.php");
	?>
	  <body>
		<div class="container">
		  <form class="form-signin">
			<div class='well'>
				<h2><b>Error</b></h2></br></br>
				<div class='alert alert-danger' role='alert'>No ha ingresado al sistema.</div>
				<meta http-equiv='Refresh' content='4;url=login.php' />
			</div>
		  </form>

		</div> <!-- /container -->
	  </body>
	<?php
	}
	?>
</html>