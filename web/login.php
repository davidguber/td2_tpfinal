<!doctype html>
<?php
  require("includes/config.php");
  ?>
<html lang="en">
  <?php
	include("includes/head2.php");
	?>
  <body class="text-center">
  <?php
		if(isset($_SESSION['Logeado']))
		{
			echo "<form class='form-signin'><div class='well'>";
			echo "<h2><b>Error</b></h2></br></br>";
			echo "<div class='alert alert-danger' role='alert'>Ya ha ingresado al sistema.</div>";
			echo "<meta http-equiv='Refresh' content='4;url=index.php' />";
			echo "</div></form>";
			
		}
		else
		{
		?>
  <form action="loginf.php" method="post" class="form-signin">
	  <img class="mb-4" src="/img/cintra.png" alt="" width="603" height="118">
	  <h1 class="h3 mb-3 font-weight-normal">Ingreso</h1>
	  <label for="usuario" class="sr-only">Usuario</label>
	  <input type="text" name="username" id="usuario" class="form-control" placeholder="Usuario" required autofocus>
	  <label for="password" class="sr-only">Contraseña</label>
	  <input type="password" name="password" id="password" class="form-control" placeholder="Contraseña" required>
	  <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
	  <?php
		include("includes/footer2.php");
		?>
  </form>
<?php
	}
	?>
		
	<?php
	mysqli_close($con);
	?>

</body>
</html>