<!DOCTYPE html>
<html lang="en">
  <?php
  require("includes/config.php");
  include("includes/head2.php");
  ?>
  <body>

    <div class="container">
      <form class="form-signin">
		<div class='well'>
			<?php
			if(isset($_SESSION['Logeado']))
			{
			unset($_SESSION['Logeado']);
			unset($_SESSION['UsuarioN']);
			unset($_SESSION['UsuarioID']);
			echo "<meta http-equiv='Refresh' content='4;url=login.php' />";
			echo "<h2><b>Saliendo del sistema</b></h2></br></br>";
			echo "<div class='alert alert-success' role='alert'>Has cerrado sesión exitosamente. Redirigiendo..</div>";
			}
			else
			{
			echo "<h2><b>Error</b></h2></br></br>
			<div class='alert alert-danger' role='alert'>No ha ingresado al sistema.</div>
			<meta http-equiv='Refresh' content='2;url=login.php'/>";
			}
		  ?>
		</div>
      </form>

    </div> <!-- /container -->
	<?php
	mysqli_close($con);
	?>
  </body>
</html>