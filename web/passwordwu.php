<!doctype html>
<html lang="en">
  <?php
	require("includes/config.php");
	if(isset($_SESSION['Logeado']))
	{ 
	include("includes/head.php");
  ?>
  
  <body>
  <?php
	include("includes/navbar.php");
	?>

<main role="main">

  <div class="container">
    <!-- Example row of columns -->
    <div class="row">
      <h1 class="page-header">Modificar usuario</h1>
          <?php
			if(empty($_POST))
			{
				$ID = $_SESSION['UsuarioID'];
				echo "
					<div class='jumbotron'>
					<div class='container'>
					  <h1 class='display-3'><b>Editar usuario</b></h1>
						<form action='passwordwu.php' method='post'>
							<h4>Contraseña Nueva (Ingresela dos veces)</h4>
							<input class='form-control' type='text' name='Password1' value=''></br>
							<input class='form-control' type='text' name='Password2' value=''></br>
							<input type='hidden' name='ID' value='".$ID."'>
							<input type='submit' class='btn btn-info' value='Confirmar' /> 
						</form>
					</div>
					</div>
				";	
			}
			elseif(!empty($_POST))
			{
				if(!empty($_POST['ID']) && !empty($_POST['Password1']) && !empty($_POST['Password2']))
				{
					$NuevoPassword1 = htmlentities(mysqli_real_escape_string($con, $_POST['Password1']));
					$NuevoPassword2 = htmlentities(mysqli_real_escape_string($con, $_POST['Password2']));
					$NuevoNombre = htmlentities(mysqli_real_escape_string($con, $_POST['Nombre']));
					$ID = mysqli_real_escape_string($con, $_POST['ID']);
					
					if (strcmp($NuevoPassword1, $NuevoPassword2) == 0)
					{
						$equery = "UPDATE `usuarios_web` SET `Password`=MD5('$NuevoPassword1') WHERE `ID`='$ID' LIMIT 1";
						$eresult=mysqli_query($con, $equery);
						if($eresult == true)
						{
							echo "
							<div class='jumbotron'>
							<div class='container'>
							<div class='alert alert-success' role='alert'>
							  <h4 class='alert-heading'>¡Exito!</h4>
							  <p>Se ha modificado el usuario con éxito. Puede volver al <a href='index.php'>inicio</a>.</p>
							</div>
							</div>
							</div>
							<meta http-equiv='Refresh' content='4;url=index.php' />
							";
						}
						else
						{
							echo "
							<div class='jumbotron'>
							<div class='container'>
							<div class='alert alert-danger' role='alert'>
								  <h4 class='alert-heading'>¡ERROR!</h4>
								  <p>Hubo un error en la página. Por favor vuelva a <a href='passwordwu.php'>intentarlo nuevamente</a>.</p>
							</div>
							</div>
							</div>
							<meta http-equiv='Refresh' content='4;url=passwordwu.php' />
							";
						}
						mysqli_free_result($eresult);
					}
					else 
					{
						echo "
						<div class='jumbotron'>
						<div class='container'>
						<div class='alert alert-danger' role='alert'>
							  <h4 class='alert-heading'>¡ERROR!</h4>
							  <p>Las contraseñas no son iguales. Por favor vuelva a <a href='passwordwu.php'>intentarlo nuevamente</a>.</p>
						</div>
						</div>
						</div>
						<meta http-equiv='Refresh' content='4;url=passwordwu.php' />
						";
					}
				}
				else
				{
					echo "
					<div class='jumbotron'>
					<div class='container'>
					<div class='alert alert-danger' role='alert'>
						  <h4 class='alert-heading'>¡ERROR!</h4>
						  <p>Hay campos vacíos. Por favor vuelva a <a href='passwordwu.php'>intentarlo nuevamente</a>.</p>
					</div>
					</div>
					</div>
					<meta http-equiv='Refresh' content='4;url=passwordwu.php' />
					";
				}
			}
			else
			{
				echo "
				<div class='jumbotron'>
				<div class='container'>
				<div class='alert alert-danger' role='alert'>
					  <h4 class='alert-heading'>¡ERROR!</h4>
					  <p>Hubo un error en la página. Por favor vuelva a <a href='passwordwu.php'>intentarlo nuevamente</a>.</p>
				</div>
				</div>
				</div>
				<meta http-equiv='Refresh' content='4;url=passwordwu.php' />
				";
			}
			?>
		  
    </div>

    <hr>

  </div> <!-- /container -->

	</main>
	<!-- Footer -->
    <?php
	include("includes/footer.php");
	?>

    <!-- Scripts -->
	<?php
	include("includes/scripts.php");
	mysqli_close($con);
	?>
	</body>
	
	<?php
	}
	else
	{
	  include("includes/head2.php");
	?>
	  <body>
		<div class="container">
		  <form class="form-signin">
			<div class='well'>
				<h2><b>Error</b></h2></br></br>
				<div class='alert alert-danger' role='alert'>No ha ingresado al sistema.</div>
				<meta http-equiv='Refresh' content='4;url=login.php' />
			</div>
		  </form>

		</div> <!-- /container -->
	  </body>
	<?php
	}
	?>
</html>