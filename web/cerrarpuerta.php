<!doctype html>
<html lang="en">
  <?php
	require("includes/config.php");
	if(isset($_SESSION['Logeado']))
	{ 
	include("includes/head.php");
  ?>
  
  <body>
  <?php
	include("includes/navbar.php");
	?>

<main role="main">

  <!-- Main jumbotron for a primary marketing message or call to action -->
  <!--<div class="jumbotron">
    <div class="container">
      <h1 class="display-3">Hello, world!</h1>
      <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
      <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
    </div>
  </div>-->

  <div class="container">
    <!-- Example row of columns -->
    <div class="row">
      <h1 class="page-header">Registro de ingresos</h1>
			<div class='jumbotron'>
				<div class='container'>
					<div class='alert alert-success' role='alert'>
					  <h4 class='alert-heading'>Puerta cerrada</h4>
					  <p>Cerrando puerta. Redirigiendo al <a href='index.php'>inicio</a>.</p>
						<?php
						echo exec('gpio mode 7 out');
						echo exec('gpio write 7 0');
						?>
					</div>
				</div>
			</div>
		<meta http-equiv='Refresh' content='4;url=index.php' />
		  
    </div>

    <hr>

  </div> <!-- /container -->

	</main>
	<!-- Footer -->
    <?php
	include("includes/footer.php");
	?>

    <!-- Scripts -->
	<?php
	include("includes/scripts.php");
	mysqli_close($con);
	?>
	</body>
	
	<?php
	}
	else
	{
	  include("includes/head2.php");
	?>
	  <body>
		<div class="container">
		  <form class="form-signin">
			<div class='well'>
				<h2><b>Error</b></h2></br></br>
				<div class='alert alert-danger' role='alert'>No ha ingresado al sistema.</div>
				<meta http-equiv='Refresh' content='4;url=login.php' />
			</div>
		  </form>

		</div> <!-- /container -->
	  </body>
	<?php
	}
	?>
</html>