<!DOCTYPE html>
<html lang="en">
  <?php
  require("includes/config.php");
  ?>
  <?php
  include("includes/head2.php");
  ?>

  <body>

    <div class="container">

      <form class="form-signin">
		<div class='well'>
		<?php
		if (empty($_POST))
		{
			echo "<h2><b>Error</b></h2></br></br>";
			echo "<div class='alert alert-danger' role='alert'>No inserto ninguna informacion.</div>";
			echo "<meta http-equiv='Refresh' content='4;url=login.php' />";
		}
		elseif (empty($_POST['username']))
		{
			echo "<h2><b>Error</b></h2></br></br>";
			echo "<div class='alert alert-danger' role='alert'>No inserto el nombre de usuario.</div>";
			echo "<meta http-equiv='Refresh' content='4;url=login.php' />";
		}
		elseif (empty($_POST['password']))
		{
			echo "<h2><b>Error</b></h2></br></br>";
			echo "<div class='alert alert-danger' role='alert'>No inserto la contraseña.</div>";
			echo "<meta http-equiv='Refresh' content='4;url=login.php' />";
		}
		elseif (!empty($_POST['username']) && !empty($_POST['password']))
		{
		$userlogin = htmlentities(mysqli_real_escape_string($con, $_POST['username']));
		$userpass = htmlentities(mysqli_real_escape_string($con, $_POST['password']));

		$query = "SELECT * FROM `usuarios_web` WHERE `Usuario`='$userlogin' AND `Password`=MD5('$userpass') LIMIT 0,1";
		$result = mysqli_query($con, $query);
		$num=mysqli_num_rows($result);

		if($num == 1)
		{
			$row=mysqli_fetch_array($result,MYSQLI_ASSOC);
			$userid = $row['ID'];
			$_SESSION['Logeado'] = 1;
			$_SESSION['UsuarioN'] = $userlogin;
			$_SESSION['UsuarioID'] = $userid;
			mysqli_free_result($result);
			$ip = $_SERVER['REMOTE_ADDR'];
			mysqli_query($con, "INSERT INTO `usuarios_web_logs`(`Usuario`, `IP`) VALUES ('$userlogin','$ip')");
			echo "<h2><b>Iniciando sesión</b></h2></br></br>";
			echo "<div class='alert alert-success' role='alert'>Ha ingresado correctamente al sistema. Redirigiendo al sistema..</div>";
			echo "<meta http-equiv='Refresh' content='4;url=index.php' />";
		}
		else
		{
			echo "<h2><b>Error</b></h2></br></br>";
			echo "<div class='alert alert-danger' role='alert'>El usuario o contraseña ingresados son incorrectos.</div>";
			echo "<meta http-equiv='Refresh' content='4;url=login.php' />";
			mysqli_free_result($result);
		}
		}
		mysqli_close($con);
	  ?>
		</div>
      </form>

    </div> <!-- /container -->

  </body>
</html>