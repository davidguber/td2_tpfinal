<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="#">CINTRA - Control de Acceso</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Inicio</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sistema</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="index.php">Registro de ingreso</a>
          <a class="dropdown-item" href="agregaru.php">Agregar usuario</a>
          <a class="dropdown-item" href="listaru.php">Lista de usuarios</a>
		  <a class="dropdown-item" href="abrirpuerta.php">Abrir puerta</a>
		  <a class="dropdown-item" href="cerrarpuerta.php">Cerrar puerta</a>
        </div>
      </li>
    </ul>
	<ul class="navbar-nav navbar-right">
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Configuracion Web</a>
			<div class="dropdown-menu" aria-labelledby="dropdown02">
			  <a class="dropdown-item" href="passwordwu.php">Cambiar Contraseña</a>
			  <a class="dropdown-item" href="agregarwu.php">Agregar usuario</a>
			  <a class="dropdown-item" href="listarwu.php">Eliminar usuario</a>
			</div>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="logout.php">Cerrar Sesión</a>
		</li>
	</ul>
  </div>
</nav>