-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 16-12-2019 a las 16:35:10
-- Versión del servidor: 10.3.17-MariaDB-0+deb10u1
-- Versión de PHP: 7.3.11-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `control_acceso`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cda_usuarios`
--

CREATE TABLE `cda_usuarios` (
  `ID` int(11) NOT NULL,
  `Usuario` int(11) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `Nombre` varchar(24) NOT NULL,
  `Apellido` varchar(24) NOT NULL,
  `Telefono` varchar(16) NOT NULL,
  `Email` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cda_usuarios`
--

INSERT INTO `cda_usuarios` (`ID`, `Usuario`, `Password`, `Nombre`, `Apellido`, `Telefono`, `Email`) VALUES
(1, 1, '81dc9bdb52d04dc20036dbd8313ed055', 'David Michel', 'Guber', '3515557989', 'davidgu200@gmail.com'),
(2, 2, '81dc9bdb52d04dc20036dbd8313ed055', 'Manuel', 'Huerga', '3541572957', 'manuhuerga@gmail.com'),
(3, 1000, '81dc9bdb52d04dc20036dbd8313ed055', 'Juan', 'Perez', '3511111111', 'juanperez@gmail.com'),
(6, 4, '912e79cd13c64069d91da65d62fbb78c', 'Fernando', 'Mallada', '3541552488', 'mallada.fernando@gmail.com'),
(7, 5, 'db1915052d15f7815c8b88e879465a1e', 'Maximiliano', 'Salvanera', '3515303673', 'maximiliano.salvanera.sk8@gmail.com'),
(8, 10, '44bf89b63173d40fb39f9842e308b3f9', 'Juan', 'Pablo', '564351654', 'ajksbdkja@hjavsjdhav');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs_entrada`
--

CREATE TABLE `logs_entrada` (
  `ID` int(11) NOT NULL,
  `Usuario` int(11) NOT NULL,
  `Momento` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `logs_entrada`
--

INSERT INTO `logs_entrada` (`ID`, `Usuario`, `Momento`) VALUES
(1, 1, '2019-11-15 21:59:56'),
(2, 2, '2019-11-15 22:03:48'),
(3, 4800129, '2019-11-15 22:09:19'),
(4, 1, '2019-11-15 22:20:30'),
(5, 1000, '2019-11-15 22:22:02'),
(6, 2, '2019-11-15 22:22:19'),
(7, 1, '2019-11-15 22:24:46'),
(8, 2, '2019-11-15 22:24:56'),
(9, 2, '2019-11-15 22:25:35'),
(10, 1, '2019-11-22 17:13:27'),
(11, 2, '2019-11-22 17:13:39'),
(12, 1000, '2019-11-22 17:32:54'),
(13, 1000, '2019-11-22 17:35:07'),
(14, 1001, '2019-11-22 19:18:34'),
(15, 1000, '2019-11-22 19:21:17'),
(16, 2, '2019-11-22 19:21:49'),
(17, 1, '2019-11-22 19:22:12'),
(18, 1, '2019-11-22 19:22:45'),
(19, 1001, '2019-11-22 19:23:33'),
(20, 1000, '2019-11-22 19:23:44'),
(21, 1, '2019-11-22 19:23:57'),
(22, 1, '2019-11-22 21:30:27'),
(23, 2, '2019-11-22 21:31:20'),
(24, 1, '2019-11-23 00:08:57'),
(25, 1, '2019-11-26 18:03:10'),
(26, 1, '2019-11-27 20:36:16'),
(27, 5, '2019-11-27 20:43:53'),
(28, 4, '2019-11-27 20:45:02'),
(29, 2, '2019-11-27 20:45:24'),
(30, 1000, '2019-11-27 20:45:35'),
(31, 1, '2019-11-27 21:35:10'),
(32, 1, '2019-11-27 21:40:50'),
(33, 2, '2019-11-27 21:43:23'),
(34, 10, '2019-11-27 21:49:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_web`
--

CREATE TABLE `usuarios_web` (
  `ID` int(11) NOT NULL,
  `Usuario` varchar(32) NOT NULL,
  `Password` varchar(128) NOT NULL,
  `Mail` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios_web`
--

INSERT INTO `usuarios_web` (`ID`, `Usuario`, `Password`, `Mail`) VALUES
(1, 'dguber', '9a483d6eef599e3e37554b481ef2a151', 'davidgu200@gmail.com'),
(4, 'mhuerga', 'e120ea280aa50693d5568d0071456460', 'manuhuerga@gmail.com'),
(5, 'fmallada', 'a3dcb4d229de6fde0db5686dee47145d', 'ferellocomallada@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_web_logs`
--

CREATE TABLE `usuarios_web_logs` (
  `ID` int(11) NOT NULL,
  `Usuario` varchar(32) NOT NULL,
  `IP` varchar(16) NOT NULL,
  `Momento` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `usuarios_web_logs`
--

INSERT INTO `usuarios_web_logs` (`ID`, `Usuario`, `IP`, `Momento`) VALUES
(1, 'dguber', '192.168.1.6', '2019-11-19 23:27:20'),
(2, 'dguber', '192.168.1.6', '2019-11-19 23:28:15'),
(3, 'dguber', '192.168.1.6', '2019-11-19 23:38:42'),
(4, 'dguber', '192.168.1.6', '2019-11-19 23:39:43'),
(5, 'dguber', '192.168.1.6', '2019-11-20 00:37:58'),
(6, 'dguber', '192.168.1.6', '2019-11-20 23:33:55'),
(7, 'dguber', '192.168.1.6', '2019-11-21 02:35:00'),
(8, 'dguber', '192.168.1.6', '2019-11-21 03:17:02'),
(9, 'dguber', '192.168.1.6', '2019-11-22 15:52:31'),
(10, 'dguber', '192.168.1.6', '2019-11-22 17:11:32'),
(11, 'dguber', '192.168.1.6', '2019-11-22 17:43:16'),
(12, 'dguber', '192.168.1.6', '2019-11-22 21:06:18'),
(13, 'mhuerga', '192.168.1.6', '2019-11-22 21:49:30'),
(14, 'mhuerga', '192.168.1.6', '2019-11-22 21:52:27'),
(15, 'dguber', '192.168.43.105', '2019-11-23 00:09:46'),
(16, 'dguber', '192.168.43.127', '2019-11-26 17:49:34'),
(17, 'dguber', '192.168.43.71', '2019-11-27 20:38:24'),
(18, 'dguber', '192.168.43.71', '2019-11-27 21:42:39');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cda_usuarios`
--
ALTER TABLE `cda_usuarios`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `logs_entrada`
--
ALTER TABLE `logs_entrada`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `usuarios_web`
--
ALTER TABLE `usuarios_web`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `usuarios_web_logs`
--
ALTER TABLE `usuarios_web_logs`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cda_usuarios`
--
ALTER TABLE `cda_usuarios`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `logs_entrada`
--
ALTER TABLE `logs_entrada`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `usuarios_web`
--
ALTER TABLE `usuarios_web`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `usuarios_web_logs`
--
ALTER TABLE `usuarios_web_logs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
